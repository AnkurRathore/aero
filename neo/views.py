from django.shortcuts import render
from django.http import HttpResponse
from django.views import View
from neo.models import TrafficSummary
import json
import urllib
import  requests
# Create your views here.

#The Function fetches the JSON Data from the the API
def fetch_data():
    apikey='7a34d95e-9c71-11e6-84c1-59169601a043'
    base_url='https://aertrafficapi.aeris.com/v1/18740/systemReports/trafficSummary?durationInDays=7&apiKey=7a34d95e-9c71-11e6-84c1-59169601a043&subAccounts=false'
    result=requests.get(base_url).json()
    return result

#View responsible for displaying the Pie Chart Template
def demo_piechart(request):
    res=fetch_data()
    response=render(request,'neo/piechart.html',{'res':res})
    return response
#View responsible for rendering the Bar Chart Template
def demo_barchart(request):
    res=fetch_data()
    response=render(request,'neo/barchart.html',{'res':res})
    return response
