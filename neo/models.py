from django.db import models

# Create your models here.

class TrafficSummary(models.Model):
    accountid=models.IntegerField(null=True)
    deviceid=models.IntegerField(null=True)
    smsMessages=models.IntegerField(null=True)
    voiceCalls=models.IntegerField(null=True)
    voiceDuration=models.IntegerField(null=True)
    date=models.DateTimeField(null=True)
    totalSMSCount=models.IntegerField(null=True)
    packetDataKb=models.DecimalField(max_digits=7,decimal_places=2,null=True,default=0.00)
