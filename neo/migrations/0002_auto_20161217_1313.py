# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2016-12-17 13:13
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('neo', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='trafficsummary',
            name='accountid',
            field=models.IntegerField(null=True),
        ),
        migrations.AlterField(
            model_name='trafficsummary',
            name='date',
            field=models.DateTimeField(null=True),
        ),
        migrations.AlterField(
            model_name='trafficsummary',
            name='deviceid',
            field=models.IntegerField(null=True),
        ),
        migrations.AlterField(
            model_name='trafficsummary',
            name='packetDataKb',
            field=models.DecimalField(decimal_places=2, max_digits=7, null=True),
        ),
        migrations.AlterField(
            model_name='trafficsummary',
            name='smsMessages',
            field=models.IntegerField(null=True),
        ),
        migrations.AlterField(
            model_name='trafficsummary',
            name='totalSMSCount',
            field=models.IntegerField(null=True),
        ),
        migrations.AlterField(
            model_name='trafficsummary',
            name='voiceCalls',
            field=models.IntegerField(null=True),
        ),
        migrations.AlterField(
            model_name='trafficsummary',
            name='voiceDuration',
            field=models.IntegerField(null=True),
        ),
    ]
