from django.conf.urls import url,include
from neo import views

urlpatterns=[
    url(r'^pie/$',views.demo_piechart,name='demo_piechart'),
    url(r'^barchart/$',views.demo_barchart,name='bar_chart')
]
